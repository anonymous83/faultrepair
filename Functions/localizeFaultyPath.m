function [CriticalFitnes, BranchVisitedWhenFailureOccurs, timeWhenBranchShouldbeVisitedAndFailureoccurs,timeWhenBranchVisitedWhenFailureoccurs]= localizeFaultyPath(SimulationSteps,NormFoverTime1,NormFoverTime2,NormFoverTime3,NormFoverTime4,NormFoverTime5,MinNormFoverTime1,MinNormFoverTime2,MinNormFoverTime3,MinNormFoverTime4,MinNormFoverTime5,Branch,BrakeOutput,BrakePPValue,BrakeAEBValue, BrakeTSRValue,BrakeACCValue,posBranch)

BranchVisitedWhenFailureOccurs=0;

ValF=[MinNormFoverTime1; MinNormFoverTime2; MinNormFoverTime3; MinNormFoverTime4; MinNormFoverTime5];
ValThr=[0.5; 0.4; 0.25; 0.3; 0.25]; %0.59
ValThrAccident=[0; 0.4; 0.25; 0.3; 0.25];

whichF=[];
k=0;

for j=1:5
    if ValF(j) <= ValThrAccident(j)
        k=k+1;
        whichF(k)=j;
    end
end

k1=0;
if k~=0
    if k~=1
        DiffValTh=[];
        for j=1:5
            if ValF(j) <= ValThrAccident(j)
                k1=k1+1;
                DiffValTh(k1)=ValThrAccident(j) - ValF(j);
            end
        end
        
        [minVal,index]=min(DiffValTh);
        CriticalFitnes=whichF(index);
    else
        CriticalFitnes=whichF(1);
    end
else
    %% a passed TC
    CriticalFitnes=0;
end


if CriticalFitnes ==1
    BranchShouldBeVisited1=1; 
    BranchShouldBeVisited2=4;
end
if CriticalFitnes ==2
    BranchShouldBeVisited1=2; 
    BranchShouldBeVisited2=5;
end
if CriticalFitnes ==3
    BranchShouldBeVisited=3;
end
if CriticalFitnes ==4
    BranchShouldBeVisited=6;
end
if CriticalFitnes ==5
    BranchShouldBeVisited=7;
end

%which branch is visited when the failure occurs

T=[];
for j=1:(SimulationSteps+1),
    T(j,:)=Branch.signals.values(j,:);
    
end

%%%%%%%%%%%%%% determine b_current different from b_f that is the most recent visited branch before the failure
timeWhenBranchShouldbeVisitedAndFailureoccurs=0;
timeWhenBranchVisitedWhenFailureoccurs=0;

branchVisited=0;
if CriticalFitnes ==1
    timeWhenThisIsHappen=0;
    for j=1:(SimulationSteps+1),
        if NormFoverTime1(j)<=ValThr(1)
            timeWhenThisIsHappen=j;
            timeWhenBranchShouldbeVisitedAndFailureoccurs=j;
            for k=1:8
                if T(j,k)~=0
                    branchVisited=k;
                end
            end
                BranchVisitedWhenFailureOccurs=find(posBranch==branchVisited);
                timeWhenBranchVisitedWhenFailureoccurs=j;

            break;
        end
    end
end
%%FD2
if CriticalFitnes ==2
    timeWhenThisIsHappen=0;
    for j=1:(SimulationSteps+1),
        if NormFoverTime2(j)<=ValThr(2)
            timeWhenThisIsHappen=j;
            timeWhenBranchShouldbeVisitedAndFailureoccurs=j;
            
            for k=1:8
                if T(j,k)~=0
                    branchVisited=k;
                end
            end
                BranchVisitedWhenFailureOccurs=find(posBranch==branchVisited);
                timeWhenBranchVisitedWhenFailureoccurs=j;
%                 

            break;
        end
    end
end
%%FD3
if CriticalFitnes ==3
    timeWhenThisIsHappen=0;
    for j=1:(SimulationSteps+1),
        if NormFoverTime3(j)<=ValThr(3)
            timeWhenThisIsHappen=j;
            timeWhenBranchShouldbeVisitedAndFailureoccurs=j;
            
            for k=1:8
                if T(j,k)~=0
                    branchVisited=k;
                end
            end
                BranchVisitedWhenFailureOccurs=find(posBranch==branchVisited);
                timeWhenBranchVisitedWhenFailureoccurs=j;

            break;
        end
    end
end
%%FD4
if CriticalFitnes ==4
    timeWhenThisIsHappen=0;
    for j=1:(SimulationSteps+1),
        if NormFoverTime4(j)<=ValThr(4)
            timeWhenThisIsHappen=j;
            timeWhenBranchShouldbeVisitedAndFailureoccurs=j;
            
            for k=1:8
                if T(j,k)~=0
                    branchVisited=k;
                end
            end
                BranchVisitedWhenFailureOccurs=find(posBranch==branchVisited);
                timeWhenBranchVisitedWhenFailureoccurs=j;

            break;
        end
    end
end
%%FD5

if CriticalFitnes ==5
    timeWhenThisIsHappen=0;
    for j=1:(SimulationSteps+1),
        if NormFoverTime5(j)<=ValThr(5)
            timeWhenThisIsHappen=j;
            timeWhenBranchShouldbeVisitedAndFailureoccurs=j;
            
            for k=1:8
                if T(j,k)~=0
                    branchVisited=k;
                end
            end
                BranchVisitedWhenFailureOccurs=find(posBranch==branchVisited);
                                timeWhenBranchVisitedWhenFailureoccurs=j;


            break;
        end
    end
end

BranchIsVisited=0;
Branch1IsVisited=0;
Branch2IsVisited=0;
if CriticalFitnes ==1 || CriticalFitnes ==2
    for j=timeWhenThisIsHappen:(SimulationSteps+1),
        if  T(j,BranchShouldBeVisited1)~=0
            Branch1IsVisited=1;
            break;
        end
    end
    for j=timeWhenThisIsHappen:(SimulationSteps+1),
        if  T(j,BranchShouldBeVisited2)~=0
            Branch2IsVisited=1;
            break;
        end
    end
else if CriticalFitnes==3 || CriticalFitnes==4 || CriticalFitnes==5
        for j=timeWhenThisIsHappen:(SimulationSteps+1),
            if  T(j,BranchShouldBeVisited)~=0
                BranchIsVisited=1;
                break;
            end
        end
    end
end


