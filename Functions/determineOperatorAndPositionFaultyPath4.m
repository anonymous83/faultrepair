function [pos, position1, position2, operator,posBranch,ThrtoChange]=determineOperatorAndPositionFaultyPath4(posBranch,BranchVisitedWhenFailureOccurs,checkVal,timeWhenBranchShouldbeVisitedAndFailureoccurs,timeWhenBranchVisitedWhenFailureoccurs)

%%% only Shift
operator=0;
opa1=0;
opa2=0;
%%%%%%%%% Decide which mutation operator to use %%%%%%%%%
if rand(1)<=0.5
    operator=1;
else
        operator=3;
end
%%%%%%%%%%%%%%%%%%%%%%%%%
if operator==1
    pos1=round(((BranchVisitedWhenFailureOccurs-1)*rand(1,1)+1));
    pos2=BranchVisitedWhenFailureOccurs;
    for tt=1:10
        aa=round(((BranchVisitedWhenFailureOccurs-1)*rand(1,1)+1));
        if pos1==aa
            aa=round(((BranchVisitedWhenFailureOccurs-1)*rand(1,1)+1));
            pos2=aa;
        else
            pos2=aa;
            
            break;
        end
    end
    if pos1 < pos2
        if  pos1==find(posBranch==1) || pos1==find(posBranch==2) %change condition in a way to go to b_f
            if  pos1==find(posBranch==1)
                opa1=1;
            else if pos1==find(posBranch==2)
                    opa2=1;
                end
            end
        else
            if pos2 == find(posBranch==4)
                if pos1 <= find(posBranch==1)
                    operator=0;
                else
                    
                        operator=3;
                end
                
            else
                if pos1 == find(posBranch==1)
                    if pos2 <= find(posBranch==4)

                            operator=3;
                        
                    else
                        operator=0;
                        
                    end
                    
                else if pos2 == find(posBranch==5)
                        if pos1 <= find(posBranch==2)
                            operator=0;
                        else
                           
                                operator=3;
                        end
                        
                    else if pos1 == find(posBranch==2)
                            if pos2 <= find(posBranch==5)
                                
                                
                                    operator=3;
                                
                            else
                                operator=0;
                                
                            end
                            
                            
                        else if  pos2 == find(posBranch==6)
                                if pos1 <= find(posBranch==3)
                                    operator=0;
                                else
                                    
                                        operator=3;
                                end
                            else   if pos1 == find(posBranch==3)
                                    if pos2 <= find(posBranch==6)
                                        
                                       
                                            operator=3;
                                        
                                    else
                                        operator=0;
                                        
                                    end
                                    
                                else
                                    
                                    
                                        operator=3;
                                end
                            end
                        end
                    end
                end
            end
            
            
        end
    else if pos1 > pos2
            
            if pos2==find(posBranch==1) || pos2==find(posBranch==2) %change condition to not to go to b_current
                
                operator=1;
                if  pos2==find(posBranch==1)
                    opa1=1;
                else if pos2==find(posBranch==2)
                        opa2=1;
                    end
                end
            else
                %swap
                if pos1 == find(posBranch==4)
                    if pos2 <= find(posBranch==1)
                        operator=0;
                    else
                        
                            operator=3;
                    end
                    
                    
                else  if pos2 == find(posBranch==1)
                        if pos1 <= find(posBranch==4)
                            
                            
                                operator=3;
                            
                        else
                            operator=0;
                            
                        end
                        
                    else if pos1 == find(posBranch==5)
                            if pos2 <= find(posBranch==2)
                                operator=0;
                            else
                                
                                    operator=3;
                            end
                            
                        else if pos2 == find(posBranch==2)
                                if pos1 <= find(posBranch==5)
                                    
                                   
                                        operator=3;
                                    
                                else
                                    operator=0;
                                    
                                end
                                
                            else if pos1 == find(posBranch==6)
                                    if pos2 <= find(posBranch==3)
                                        operator=0;
                                    else
                                       
                                            operator=3;
                                    end
                                    
                                else   if pos2 == find(posBranch==3)
                                        if pos1 <= find(posBranch==6)
                                            
                                            
                                                operator=3;
                                            
                                        else
                                            operator=0;
                                            
                                        end
                                        
                                    else
                                        
                                        
                                      
                                            operator=3;
                                    end
                                end
                            end
                        end
                    end
                end
            end
        else
            operator =0;
        end
    end
    
    %%%%%%%%%%%%%
else
    pos1=round(((7-1)*rand(1,1)+1));
    pos2=BranchVisitedWhenFailureOccurs;
    
    for tt=1:10
        aa=round(((7-1)*rand(1,1)+1));
        if pos1==aa
            aa=round(((7-1)*rand(1,1)+1));
            pos2=aa;
        else
            pos2=aa;
            
            break;
        end
    end
    
        if pos1 < pos2
            
            if pos2 == find(posBranch==4)
                if pos1 <= find(posBranch==1)
                    operator=0;
                else
                    operator=3;
                    
                end
                
            else if pos1 == find(posBranch==1)
                    if pos2 <= find(posBranch==4)
                        
                        operator=3;
                    else
                        operator=0;
                        
                    end
                    
                else if pos2 == find(posBranch==5)
                        if pos1 <= find(posBranch==2)
                            operator=0;
                        else
                            
                            
                            operator=3;
                            
                        end
                    else if pos1 == find(posBranch==2)
                            if pos2 <= find(posBranch==5)
                                
                                operator=3;
                            else
                                operator=0;
                                
                            end
                            
                            
                        else  if pos2 == find(posBranch==6)
                                if pos1 <= find(posBranch==3)
                                    operator=0;
                                else
                                    operator=3;
                                    
                                end
                                
                            else if pos1 == find(posBranch==3)
                                    if pos2 <= find(posBranch==6)
                                        
                                        operator=3;
                                    else
                                        operator=0;
                                        
                                    end
                                    
                                else
                                    
                                    
                                    
                                    operator=3;
                                    
                                end
                            end
                        end
                    end
                end
            end
            
            
            
        else if pos1 > pos2
                
                %swap
                if pos1 == find(posBranch==4)
                    if pos2 <= find(posBranch==1)
                        operator=0;
                    else
                        
                        operator=3;
                        
                    end
                    
                else if pos2 == find(posBranch==1)
                        if pos1 <= find(posBranch==4)
                            
                            operator=3;
                        else
                            operator=0;
                            
                        end
                        
                    else if pos1 == find(posBranch==5)
                            if pos2 <= find(posBranch==2)
                                operator=0;
                            else
                                
                                operator=3;
                                
                            end
                            
                            
                        else if pos2 == find(posBranch==2)
                                if pos1 <= find(posBranch==5)
                                    
                                    operator=3;
                                else
                                    operator=0;
                                    
                                end
                                
                                
                            else if pos1 == find(posBranch==6)
                                    if pos2 <= find(posBranch==3)
                                        operator=0;
                                    else
                                        
                                        operator=3;
                                        
                                    end
                                    
                                else if pos2 == find(posBranch==3)
                                        if pos1 <= find(posBranch==6)
                                            
                                            operator=3;
                                        else
                                            operator=0;
                                            
                                        end
                                        
                                    else
                                        
                                        
                                        operator=3;
                                        
                                    end
                                end
                            end
                        end
                    end
                end
                
            else
                operator =0;
            end
            
        end
end




% if we will apply operator 1: determine the position of the branch to change
% pos1=find(posBranch==whichBranchToChange);
% pos2=find(posBranch==BranchVisitedWhenFailureOccurs);


if  operator==1
    pos=min(pos1,pos2);
    %pos=min(pos1,pos2);
    position1=1; %do not switch
    position2=1; %do not switch
% else if operator==2
%         pos=BranchVisitedWhenFailureOccurs;
%         position1=min(pos1,pos2);
%         position2=max(pos1,pos2);
%         
     else % to verify (operator3)
        
        pos=BranchVisitedWhenFailureOccurs;
        %
        position1=min(pos1,pos2);
        position2=max(pos1,pos2);
    end


%%%%%%%%% update the order of the branches %%%%%%%%%
%b1 pos=1;
%b2 pos=2;
%b3 pos=3;
%b4 pos=4;
%b5 pos=5;
%b6 pos=6;
%b7 pos=7;

%if we decide to change condition, we keep the same order of the branches
if operator==2
    %if we decide to swap whichBranchToChange and BranchVisitedWhenFailureOccurs
    if pos1~=pos2
        for i=1:7
            if i== pos1
                temp1=posBranch(pos1);
                posBranch(pos1)=posBranch(pos2);
                posBranch(pos2)=temp1;
            end
        end
    end

    %if we decide to delete and insert condition in another place (before the BranchVisitedWhenFailureOccurs) (only if whichBranchToChange is located after BranchVisitedWhenFailureOccurs )
else if operator==3
        
        if pos1 > pos2
            ppN=zeros(1,7);
            for i=1:7
                if i==pos2
                    ppN(i)=posBranch(pos1);%whichBranchToChange;
                    for j=i+1:7
                        if j<= pos1
                            ppN(j)=posBranch(j-1);
                        end
                        if j> pos1
                            ppN(j)=posBranch(j);
                        end
                    end
                    break;
                else
                    ppN(i)=posBranch(i);
                end
            end
            posBranch=ppN;
        else
            ppN=zeros(1,7);
            for i=1:7
                if i==pos1
                    ppN(i)=posBranch(pos2);%whichBranchToChange;
                    for j=i+1:7
                        if j<= pos2
                            ppN(j)=posBranch(j-1);
                        end
                        if j> pos2
                            ppN(j)=posBranch(j);
                        end
                    end
                    break;
                else
                    ppN(i)=posBranch(i);
                end
            end
            posBranch=ppN;
            
        end
       
        
    end
end


ValuesToCheck=[];
ThrtoChange=0;
if operator==1
    if opa1==1
        
        if rand <=0.3
            ThrtoChange=1;
        else if rand <=0.7
                ThrtoChange=2;
            else
                ThrtoChange=3;
            end
        end

        
    else if opa2==1
            if rand(1)<=0.5
                ThrtoChange=4;
            else
                ThrtoChange=5;
            end
        end
    end
end