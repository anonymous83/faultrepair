

function [MinNormFoverTime1 , MinNormFoverTime2 ,MinNormFoverTime3, MinNormFoverTime4 , MinNormFoverTime5,NormFoverTime1, NormFoverTime2, NormFoverTime3, NormFoverTime4, NormFoverTime5]=computeMinFOverTime3(desiredSpeed,Input,SimulationSteps,xPerson,xCar,yPerson, yCar, xLeadCar,xStop,xSpeedLimit,vCar,v0L)



SpeedLimitMin=100;
for j=1:size(desiredSpeed.signals.values,1)
    if (desiredSpeed.signals.values(j,1)~=0.1) && ( desiredSpeed.signals.values(j,1)~=0) && ( round(desiredSpeed.signals.values(j,1))~=round(0.0278))
        if desiredSpeed.signals.values(j,1) <SpeedLimitMin
            SpeedLimitMin= desiredSpeed.signals.values(j,1);
        end
    end
end
SpeedLimit=SpeedLimitMin;

%F over time
[FoverTime1,FoverTime2,FoverTime3,FoverTime4,FoverTime5, NormFoverTime1, NormFoverTime2, NormFoverTime3, NormFoverTime4, NormFoverTime5]=FoverTimeFR3(Input, ...
    1,SimulationSteps,xPerson,xCar,yPerson, yCar, xLeadCar,xStop,xSpeedLimit,vCar,v0L,SpeedLimit);
%%%

MinNormFoverTime1=1;
MinNormFoverTime2=1;
MinNormFoverTime3=1;
MinNormFoverTime4=1;
MinNormFoverTime5=1;

for i=1:(SimulationSteps+1),
    if NormFoverTime1(i,1) < MinNormFoverTime1
        MinNormFoverTime1= NormFoverTime1(i,1);
    end
end

for i=1:(SimulationSteps+1),
    if NormFoverTime2(i,1) < MinNormFoverTime2
        MinNormFoverTime2= NormFoverTime2(i,1);
    end
end
for i=1:(SimulationSteps+1),
    if NormFoverTime3(i,1) < MinNormFoverTime3
        MinNormFoverTime3= NormFoverTime3(i,1);
    end
end
for i=1:(SimulationSteps+1),
    if NormFoverTime4(i,1) < MinNormFoverTime4
        MinNormFoverTime4= NormFoverTime4(i,1);
    end
end
for i=1:(SimulationSteps+1),
    if NormFoverTime5(i,1) < MinNormFoverTime5
        MinNormFoverTime5= NormFoverTime5(i,1);
    end
end