function [CF]= checkInput(MinNormFoverTime1,MinNormFoverTime2,MinNormFoverTime3,MinNormFoverTime4,MinNormFoverTime5)


ValF=[MinNormFoverTime1; MinNormFoverTime2; MinNormFoverTime3; MinNormFoverTime4; MinNormFoverTime5];

ValThr=[0.59; 0.4; 0.25; 0.3; 0.25];
ValThrAccident=[0; 0.4; 0.25; 0.3; 0.25];

whichF=[];
k=0;

for j=1:5
    if ValF(j) <= ValThrAccident(j)
        k=k+1;
        whichF(k)=j;
    end
end

k1=0;
if k~=0
    if k~=1
        DiffValTh=[];
        for j=1:5
            if ValF(j) <= ValThrAccident(j)
                k1=k1+1;
                DiffValTh(k1)=ValThrAccident(j) - ValF(j);
            end
        end
        
        [minVal,index]=min(DiffValTh);
        CF=whichF(index);
    else
        CF=whichF(1);
    end
else
    %% a passed TC
    CF=0;
end