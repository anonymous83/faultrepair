# Automated Repair of Integration Rules in Automated Driving Systems

[Overview](#overview)


---
# Overview
ARIAL is a tool for resolving integration faults in automated driving systems (ADS). ARIAL localizes faults over several lines of code to fix complex integration faults and deals with the scalability issues of testing and repairing ADS. ARIAL relies on a many-objective, single-state search algorithm that uses an archive to keep track of partial repairs

- _ARIAL.m_ executes the ARIAL algorithm and repairs the integration faults.

ARIEL has been applied to two industry ADS case studies AutoDrive1 and AutoDrive2. "Case Studies" contains these two case study systems. The case studies are developed in Matlab/Simulink. 
To execute AutoDrive1 and AutoDrive2, a simulation environment called [PreScan](https://tass.plm.automation.siemens.com/prescan) is needed.

