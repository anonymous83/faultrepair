
mfilepath=fileparts(which('ARIEL.m')); 
addpath(fullfile(mfilepath,'/Functions'));

tic
CounterRestart=10;


%%%%%%positive test cases
%PP
Input1=[62.974868    35.461082   160.267935  1.440677	11.702518	1	14	80	56.049661	-1 1]; %collision between a car and a pedestrian %% At the moment of failure, TSR Stop is prioritized over PP
%AEB
Input2=[72.974868    35.461082  90.267935 4.6325	14.317466	-1	3.317466	36.107071	36.404466	-1 0]; %Collision between two cars %% At the moment of failure, TSR Stop is prioritized over AEB
%TSR Stop
Input3=[61.894868    36.461082   162.267935 4.6325	16.317466	-1	8.317466	56.107071	39.404466	-1 0]; %Do not stop at the stop sign %% At the moment of failure, AEB is prioritized over TSR Stop
%TSR SL
Input4=[52.078853	36.339775	80.093522	3.230829	10.75981	-1	12.525338	29.509662	-1	36.153169 1];%Do not respect the speed Limit %% At the moment of failure, ACC is prioritized over TSR SL

%%%%%negativetest cases
%PP
Input5=[37.382494	34.695901	147.594816	1.440677	11.702518	1	14	80	56.049661	-1 1]; % Avoid the collision between a car and a pedestrian
%Stop TSR
Input6=[61.074866    35.361082   161.267935 4.6325	16.317466	-1	8.317466	56.107071	49.404466	-1 0]; %Stop at the stop sign
%%% AEB
Input7=[82.974868    37.461082   60.267935 4.6325	16.317466	-1	8.317466	36.107071	42.404466	-1 0]; %Avoid the collision between two cars
%%% SL
Input8=[69.974868    35.461082   170.267935 1.543403	10.096506	1	8.862034	48.292591	-1	39.707362 0]; %Respect the speed limit



simulationCount=0;

Input=[Input1; Input2; Input3 ; Input4; Input5; Input6; Input7; Input8];
formatOut = 'yyyymmdd_HHMMss';
ds=datestr(now,formatOut);
name1 = strcat('muResultsAlgo_',ds,'.csv');
fid1 = fopen(fullfile(PATH+'\Demo_TA2B\OutputResults',name1), 'w');

name2=  strcat('muIntC_',ds,'.csv');
fid2 = fopen(fullfile(PATH+'\Demo_TA2B\OutputResults',name2), 'w');

name3=  strcat('muBestIntC_',ds,'.csv');
fid3 = fopen(fullfile(PATH+'\Demo_TA2B\OutputResults',name3), 'w');

name4=  strcat('Metrics_',ds,'.csv');
fid4 = fopen(fullfile(PATH+'\Demo_TA2B\OutputResults',name4), 'w');

name5=  strcat('muArchiveData_',ds,'.csv');
fid5 = fopen(fullfile(PATH+'\Demo_TA2B\OutputResults',name5), 'w');


clear EC
EC(:,1:11)=Input;
clear a;
for i=1:size(EC,1)
    a(:,i)=EC(i,:);
end
fprintf(fid1, 'Input \n');
fprintf(fid1, '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s \n',['x0P' ',' 'y0P' ',' 'ThP' ',' 'v0P' ',' 'v0C' ',' 'Fog' ',' 'v0L' ',' 'x0L' ',' 'xStop' ',' 'xSpeedLimit']);
fprintf(fid1, '\n');
fprintf(fid1, '%.6f,%.6f,%.6f,%.6f,%.6f,%d,%.6f,%.6f,%.6f,%.6f \n', a);

iter=0;
%%% INTC contains the oroginal IntC
open_system('Demo_TA2B_cs');
sf = sfroot();
Experiment_Name='Demo_TA2B';
Simulink_ModelName= [ Experiment_Name '_cs'];
a=find_system(Simulink_ModelName,'BlockType','SubSystem','Name','MATLAB Function1');
block = sf.find('Path',a{1}','-isa','Stateflow.EMChart');
fileId = fopen('case1.m');
posBranch=[1 2 3 4 5 7 6]; %for case1
%fileId = fopen('case2.m');
%posBranch=[1 3 2 5 6 7 4];%%for case2

opPoints = fscanf(fileId, '%c');
block.Script =sprintf('%c', opPoints);


fprintf(fid2, 'Iteration %d \n', iter);
fprintf(fid2, '%s',opPoints);
fprintf(fid2, '\n');

open_system('Demo_TA2B_cs');
sf = sfroot();
Experiment_Name='Demo_TA2B';
Simulink_ModelName= [ Experiment_Name '_cs'];
a=find_system(Simulink_ModelName,'BlockType','SubSystem','Name','MATLAB Function1')
block = sf.find('Path',a{1}','-isa','Stateflow.EMChart');


fileScript= block.Script;
fid = fopen('BestIntC.m', 'wt');
fprintf(fid, '%s', fileScript);
fclose(fid);

fprintf(fid3, 'Iteration %d \n', iter);
fprintf(fid3, '%c',fileScript);
fprintf(fid3, '\n');
OFMAX=100; %to check
OF=0;
highestFitness=0;
whichInputHasTheHighestFitness=1;
tableData=[];
cf=0;
critFit=0;
FitnessTC=[];

clear FitnessTC
clear FitnessTCj
clear FTC
clear FTCj
clear FitnessTC1
clear FitnessTC2
clear FitnessTC3
clear FitnessTC4
%%Run-Evaluate
CRF=[];
for i=1:size(Input,1)
    x0P=Input(i,1);
    y0P=Input(i,2);
    ThP=Input(i,3);
    v0P=Input(i,4);
    v0C=Input(i,5);
    z0P=0;
    v0L=Input(i,7);
    x0L= Input(i,8);
    
    %%%%%%%%% Generate the experiment %%%%%%%%%
    disp('Variables reinitialized...');
    disp('------------------------');
    startTime = rem(now,1);
    startTimeA = rem(now,1);
    Experiment_Name='Demo_TA2B'; % Hereby enter your experiment name
    PreScan_ExperimentName= [Experiment_Name '.pex']; % add the extension for the PreScan file
    Simulink_ModelName= [ Experiment_Name '_cs'];% add the extension for the Simulink file
    PreScan_ExperimentLocation = [pwd '\' PreScan_ExperimentName];
    simulationCount=0;
    k=0;
    A = cell(7,1);
    B = cell(7,1);
    NbrOfTagAndVal=0;
    
    
    NbrOfTagAndVal=NbrOfTagAndVal+1;
    A{NbrOfTagAndVal}='PositionX0P';
    radiusvalue={num2str(Input(i,1))};
    idx = randi(numel(radiusvalue),1) ;
    currentName = radiusvalue{idx};
    B{NbrOfTagAndVal}=currentName;
    
    
    NbrOfTagAndVal=NbrOfTagAndVal+1;
    A{NbrOfTagAndVal}='PositionY0P';
    radiusvalue={num2str(Input(i,2))};
    idx = randi(numel(radiusvalue),1) ;
    currentName = radiusvalue{idx};
    B{NbrOfTagAndVal}=currentName;
    
    NbrOfTagAndVal=NbrOfTagAndVal+1;
    A{NbrOfTagAndVal}='OrientationThP';
    radiusvalue={num2str(Input(i,3))};
    idx = randi(numel(radiusvalue),1) ;
    currentName = radiusvalue{idx};
    B{NbrOfTagAndVal}=currentName;
    NbrOfTagAndVal=NbrOfTagAndVal+1;
    
    if Input(i,11)==1
        A{NbrOfTagAndVal}='EnableFog';
        enablefog = {'True'};
        clear idx currentName
        idx = randi(numel(enablefog),1) ;
        currentName = enablefog{idx};
        B{NbrOfTagAndVal}=currentName;
    else
        A{NbrOfTagAndVal}='EnableFog';
        enablefog = {'False'};
        clear idx currentName
        idx = randi(numel(enablefog),1) ;
        currentName = enablefog{idx};
        B{NbrOfTagAndVal}=currentName;
        
    end
    if Input(i,6)==-1
        NbrOfTagAndVal=NbrOfTagAndVal+1;
        A{NbrOfTagAndVal}='FogType';
        fogtype = {'DimGray'};
        clear idx currentName
        idx = randi(numel(fogtype),1) ;
        currentName = fogtype{idx};
        B{NbrOfTagAndVal}=currentName;
    end
    
    if Input(i,6)==0
        NbrOfTagAndVal=NbrOfTagAndVal+1;
        A{NbrOfTagAndVal}='FogType';
        fogtype = {'DarkGray'};
        clear idx currentName
        idx = randi(numel(fogtype),1) ;
        currentName = fogtype{idx};
        B{NbrOfTagAndVal}=currentName;
    end
    
    if Input(i,6)==1
        NbrOfTagAndVal=NbrOfTagAndVal+1;
        A{NbrOfTagAndVal}='FogType';
        fogtype = {'Silver'};
        clear idx currentName
        idx = randi(numel(fogtype),1) ;
        currentName = fogtype{idx};
        B{NbrOfTagAndVal}=currentName;
    end
    
    NbrOfTagAndVal=NbrOfTagAndVal+1;
    A{NbrOfTagAndVal}='StopSignX';
    radiusvalue={num2str(Input(i,9))};
    idx = randi(numel(radiusvalue),1) ;
    currentName = radiusvalue{idx};
    B{NbrOfTagAndVal}=currentName;
    
    NbrOfTagAndVal=NbrOfTagAndVal+1;
    A{NbrOfTagAndVal}='SpeedLimitX';
    radiusvalue={num2str(Input(i,10))};
    idx = randi(numel(radiusvalue),1) ;
    currentName = radiusvalue{idx};
    B{NbrOfTagAndVal}=currentName;
    
    for m=1:NbrOfTagAndVal
        clear Tag Val
        Tag(m,:) =A{m};
        Val(m,:)=B{m};
        dos( ['PreScan.CLI.exe -convertAndLoad ' PreScan_ExperimentLocation ' -set ' Tag(m,:) '=' Val(m,:)]);     % Change the value in PreScan
        dos( 'PreScan.CLI.exe -save');         % Save the PreScan experiment
        
    end
    
    dos ('PreScan.CLI.exe --realignPaths');
    dos( 'PreScan.CLI.exe -build');
    
    open_system(Simulink_ModelName);                 % Open the Simulink Model
    
    regenButtonHandle = find_system(Simulink_ModelName, 'FindAll', 'on', 'type', 'annotation','text','Regenerate');
    regenButtonCallbackText = get_param(regenButtonHandle,'ClickFcn');
    
    eval(regenButtonCallbackText);
    
    xStopS=Input(i,9);
    
    %%%%%%%%% run the experiment %%%%%%%%%
    sim(fullfile(mfilepath,'/Demo_TA2B_cs.mdl'));
    
    maxSimulationsPerViewer=5;
    
    closeViewer = mod(simulationCount + 1, maxSimulationsPerViewer) == 0; % Close viewer is true if Simulation is > than maxSimulationsPerViewer
    simulationCount = simulationCount + 1;
    
    % Close viewer, if needed,  to release memory
    if (closeViewer)
        visViewer = 5;
        maxWaitTimeMS = 10*1000;
        nl.tno.wt.prescan.utils.ProcessManagement.Stop(visViewer, maxWaitTimeMS);
        pause(0.5);
        nl.tno.wt.prescan.utils.ProcessManagement.Start(5, maxWaitTimeMS);
    end
    
    %%%
    AA=SimStopTime.time;
    b=numel(AA);
    TotSim=AA(b);
    SimulationTimeStep=0.005;
    SimulationSteps=TotSim/SimulationTimeStep;
    xStop=Input(i,9);
    xSpeedLimit=Input(i,10);
    %%%% compute fi %%%%
    [MinNormFoverTime1 , MinNormFoverTime2 ,MinNormFoverTime3, MinNormFoverTime4 , MinNormFoverTime5,NormFoverTime1, NormFoverTime2, NormFoverTime3, NormFoverTime4, NormFoverTime5]=computeMinFOverTime3(desiredSpeed,Input(i,:),SimulationSteps,xPerson,xCar,yPerson, yCar, xLeadCar,xStop,xSpeedLimit,vCar,v0L);
    FTC(i)=MinNormFoverTime1 + MinNormFoverTime2 + MinNormFoverTime3 + MinNormFoverTime4 + MinNormFoverTime5;
    
    FitnessTC1(i)=MinNormFoverTime1;
    FitnessTC2(i)=MinNormFoverTime2;
    FitnessTC3(i)=MinNormFoverTime3;
    FitnessTC4(i)=MinNormFoverTime4;
    
    OF=OF+FTC(i); 
    fprintf(fid1, '\n');
    fprintf(fid1, 'FitnessInput %d ', i);
    fprintf(fid1, 'FitnessTC1 %0.6f \n', FitnessTC1(i));
    fprintf(fid1, 'FitnessTC2 %0.6f \n', FitnessTC2(i));
    fprintf(fid1, 'FitnessTC3 %0.6f \n', FitnessTC3(i));
    fprintf(fid1, 'FitnessTC4 %0.6f \n', FitnessTC4(i));
    
    
    if i==1
        tableData(1,1)=SimulationSteps;
        tableData(1:SimulationSteps+1,2)=NormFoverTime1;
        tableData(1:SimulationSteps+1,3)=NormFoverTime2;
        tableData(1:SimulationSteps+1,4)=NormFoverTime3;
        tableData(1:SimulationSteps+1,5)=NormFoverTime4;
        tableData(1:SimulationSteps+1,6)=NormFoverTime5;
        tableData(1,7)=MinNormFoverTime1;
        tableData(1,8)=MinNormFoverTime2;
        tableData(1,9)=MinNormFoverTime3;
        tableData(1,10)=MinNormFoverTime4;
        tableData(1,11)=MinNormFoverTime5;
        tableData(1:SimulationSteps+1,12:19)=Branch.signals.values(1:SimulationSteps+1,1:8);
        tableData(1:SimulationSteps+1,20)=BrakeOutput.signals.values(1:SimulationSteps+1,1);
        tableData(1:SimulationSteps+1,21)=BrakePPValue.signals.values(1:SimulationSteps+1,1);
        tableData(1:SimulationSteps+1,22)=BrakeAEBValue.signals.values(1:SimulationSteps+1,1);
        tableData(1:SimulationSteps+1,23)=BrakeTSRValue.signals.values(1:SimulationSteps+1,1);
        tableData(1:SimulationSteps+1,24)=BrakeACCValue.signals.values(1:SimulationSteps+1,1);
        tableData(1:SimulationSteps+1,25:29)=checkVal.signals.values(1:SimulationSteps+1,1:5);
        indT(i,1)=1;
        indT(i,2)=SimulationSteps+1;
    else
        SimulationSteps=size(NormFoverTime1,1) -1;
        indT(i,1)=size(tableData,1)+1;
        indT(i,2)=size(tableData,1)+SimulationSteps+1;
        
        
        tableData(indT(i,1),1)=SimulationSteps;
        tableData(indT(i,1):indT(i,2),2)=NormFoverTime1;
        tableData(indT(i,1):indT(i,2),3)=NormFoverTime2;
        tableData(indT(i,1):indT(i,2),4)=NormFoverTime3;
        tableData(indT(i,1):indT(i,2),5)=NormFoverTime4;
        tableData(indT(i,1):indT(i,2),6)=NormFoverTime5;
        tableData(indT(i,1),7)=MinNormFoverTime1;
        tableData(indT(i,1),8)=MinNormFoverTime2;
        tableData(indT(i,1),9)=MinNormFoverTime3;
        tableData(indT(i,1),10)=MinNormFoverTime4;
        tableData(indT(i,1),11)=MinNormFoverTime5;
        tableData(indT(i,1):indT(i,2),12:19)=Branch.signals.values(1:SimulationSteps+1,1:8);
        tableData(indT(i,1):indT(i,2),20)=BrakeOutput.signals.values(1:SimulationSteps+1,1);
        tableData(indT(i,1):indT(i,2),21)=BrakePPValue.signals.values(1:SimulationSteps+1,1);
        tableData(indT(i,1):indT(i,2),22)=BrakeAEBValue.signals.values(1:SimulationSteps+1,1);
        tableData(indT(i,1):indT(i,2),23)=BrakeTSRValue.signals.values(1:SimulationSteps+1,1);
        tableData(indT(i,1):indT(i,2),24)=BrakeACCValue.signals.values(1:SimulationSteps+1,1);
        tableData(indT(i,1):indT(i,2),25:29)=checkVal.signals.values(1:SimulationSteps+1,1:5);
        
    end
    critFit= checkInput(MinNormFoverTime1,MinNormFoverTime2,MinNormFoverTime3,MinNormFoverTime4,MinNormFoverTime5);
    
    if critFit~=0
        cf=cf+1;
        CRF(i)=1;
    else
        CRF(i)=0;
    end
    
end
minFitnessTC1=min(FitnessTC1);
minFitnessTC2=min(FitnessTC2);
minFitnessTC3=min(FitnessTC3);
minFitnessTC4=min(FitnessTC4);
FitnessTC=[minFitnessTC1 minFitnessTC2 minFitnessTC3 minFitnessTC4];
fprintf(fid1, 'Min FitnessTC %0.6f  %0.6f  %0.6f  %0.6f \n', FitnessTC);

fprintf(fid1, 'OF = %0.6f \n', OF);
fprintf(fid1, 'number of failed test cases %d \n', cf);




save tableDataALLBest.txt tableData -ascii
indTBest=indT;
HigherOF=OF;
fprintf(fid1, 'HigherOF = %0.6f \n', HigherOF);
SimTicToc=toc

clear a;
clear FitCrit
clear BestFitCrit
a(1,1)=iter;
a(1,2)=SimTicToc;
a(1,3)=HigherOF;
a(1,4)=cf;
fprintf(fid4, '%s,%s,%s,%s \n',['Iteration' ',' 'time(s)' ',' 'HigherOF' ',' 'cf']);
fprintf(fid4, '\n');
fprintf(fid4, '%d,%.6f,%.6f,%d \n', a);

FitnessTCBest=FTC;
BestCRF=CRF;

FitCrit(:,1)=FTC;
FitCrit(:,2)=CRF;

BestFitCrit(:,1)=FTC;
BestFitCrit(:,2)=CRF;

posBranchBestIntC=posBranch;

firstIter=1;
c2=1;
ThereIsCorrection=0;
A22=[];

open_system('Demo_TA2B_cs');
sf = sfroot();
Experiment_Name='Demo_TA2B';
Simulink_ModelName= [ Experiment_Name '_cs'];
a=find_system(Simulink_ModelName,'BlockType','SubSystem','Name','MATLAB Function1')
block = sf.find('Path',a{1}','-isa','Stateflow.EMChart');
fileScript= block.Script;

TotalSizeArchive=0;
%%%save the parent in the Archive 
TotalSizeArchive=TotalSizeArchive+1;
numP=1;
nameP=  strcat('Parent',int2str(numP),'.m');
fid = fopen(nameP, 'wt');

fprintf(fid, '%s', fileScript);
fclose(fid);

clear BestFitCritParentx
clear BestFitCritParent1
clear posBranchParent
clear posBranchBestIntCParent

assignin('base',strcat('BestFitCritParent',int2str(numP)),BestFitCrit)

tableDataALLBestx=strcat('tableDataALLBest',int2str(numP));
nametable=strcat(tableDataALLBestx,'.txt');
save(nametable,'tableData','-ascii')

assignin('base',strcat('indTBestParent',int2str(numP)),indT);

posBranchBestIntCParent(numP,:)=posBranch;
posBranchParent(numP,:)=posBranch;

assignin('base',strcat('FitnessTCParent',int2str(numP)),FitnessTC);


nbrC=0;
BestFitCritParentx=strcat('BestFitCritParent',int2str(numP));
BValCritP=eval(BestFitCritParentx);

for i=1:size(BValCritP,1)
    if BValCritP(i,2)==1
        nbrC=nbrC+1;
    end
end
assignin('base',strcat('nbrFailingParent',int2str(numP)),nbrC);

nbrFailingParentx=strcat('nbrFailingParent',int2str(numP));
BestNumbFailing=eval(nbrFailingParentx);
clear IsInArchive


IsInArchive(1)=1;
SizeArc=0;

fprintf(fid5, 'SimTicToc : %0.6f \n', SimTicToc);
fprintf(fid5, 'First element in archive \n');
fprintf(fid1, 'posBranchParent %d %d %d %d %d %d %d \n', posBranchParent(1,:));
fprintf(fid5, 'number of failed test cases %d \n', cf);
fprintf(fid5, 'Min FitnessTC %0.6f  %0.6f  %0.6f  %0.6f \n', FitnessTC);
fprintf(fid5, 'TotalSizeArchive %d \n', TotalSizeArchive);
fprintf(fid5, 'IsInArchive %d \n', IsInArchive(1));
for h=1:numP
    if IsInArchive(h)==1
        SizeArc=SizeArc+1;
    end
end
BestSizeArc=SizeArc;
BestNumbFailing=nbrC;
countImpro=0;
while ~(SizeArc==1 && BestNumbFailing==0)
    Rselect=[];
    SimTicToc=toc
    
    fprintf(fid1, 'SimTicToc : %0.6f \n', SimTicToc);
    iter=iter+1;
    countImpro=countImpro+1;
    
    fprintf(fid1, 'Iteration : %d \n', iter);
    fprintf(fid1, 'countImpro : %d \n', countImpro);
    %select a parent
    
    x=round(((TotalSizeArchive-1)*rand(1,1)+1));
    ss=0;
    if IsInArchive(x)~=1
        for u=1:10
            
            x=round(((TotalSizeArchive-1)*rand(1,1)+1));
            
            if IsInArchive(x)==1
                ss=1;
                break;
            end
        end
    end
    if ss==0
        for i=1:size(IsInArchive)
            if  IsInArchive(i)==1
                x=i;
            end
        end
    end
    clear nameP
    
    fprintf(fid1, 'Parent selected is number : %d \n', x);
    nameP=  strcat('Parent',int2str(x),'.m');
    BestFitCritParentx=strcat('BestFitCritParent',int2str(x));
    
    %%replace intC by Parent X
    open_system('Demo_TA2B_cs');
    sf = sfroot();
    Experiment_Name='Demo_TA2B';
    Simulink_ModelName= [ Experiment_Name '_cs'];
    a=find_system(Simulink_ModelName,'BlockType','SubSystem','Name','MATLAB Function1')
    block = sf.find('Path',a{1}','-isa','Stateflow.EMChart');
    
    nameP=  strcat('Parent',int2str(x),'.m');
    fileId = fopen(nameP);
    opPoints = fscanf(fileId, '%c');
    block.Script =sprintf('%c', opPoints);
    
    BestPar=  strcat(PATH+'/Demo_TA2B/Parent',int2str(x),'.m')
    
    
    BCP=eval(BestFitCritParentx);
    ii = BCP(:,2) > 0;
    A2 = BCP(ii,:);
    
    sum=0;
    for k=1:size(A2,1)
        sum=sum+A2(k,1);
    end
    
    for kk=1:size(A2,1)
        Rselect(kk,1)=A2(kk,1)/sum;
    end
    clear BestFitCritWithProb
    BestFitCritWithProb(:,1)=1-Rselect(:,1);
    BestFitCritWithProb(:,2)= A2(:,1);
    
    indHighprob= RouletteWheelSelection(BestFitCritWithProb(:,1));
    val=BestFitCritWithProb(indHighprob,2);
    
    idx= find(BCP(:,1)==val);
    InputToMutate=Input(idx,:);
    if size(idx,1)>1
        idx=idx(1);
    end
    fprintf(fid1, 'Input responsible for the failure is number : %d \n', idx);
    
    
    tableDataALLBestx=strcat('tableDataALLBest',int2str(x));
    nametable=strcat(tableDataALLBestx,'.txt');
    TableData=importdata(nametable,' ');
    indTBestParentx=strcat('indTBestParent',int2str(x));
    indTBest=eval(indTBestParentx);
    
    SimulationSteps=TableData(indTBest(idx,1),1);
    NormFoverTime1=TableData(indTBest(idx,1):indTBest(idx,2),2);
    NormFoverTime2=TableData(indTBest(idx,1):indTBest(idx,2),3);
    NormFoverTime3=TableData(indTBest(idx,1):indTBest(idx,2),4);
    NormFoverTime4=TableData(indTBest(idx,1):indTBest(idx,2),5);
    NormFoverTime5=TableData(indTBest(idx,1):indTBest(idx,2),6);
    MinNormFoverTime1=TableData(indTBest(idx,1),7);
    MinNormFoverTime2=TableData(indTBest(idx,1),8);
    MinNormFoverTime3=TableData(indTBest(idx,1),9);
    MinNormFoverTime4=TableData(indTBest(idx,1),10);
    MinNormFoverTime5=TableData(indTBest(idx,1),11);
    Branch.signals.values(1:SimulationSteps+1,1:8)=TableData(indTBest(idx,1):indTBest(idx,2),12:19);
    BrakeOutput.signals.values(1:SimulationSteps+1,1)=TableData(indTBest(idx,1),20);
    BrakePPValue.signals.values(1:SimulationSteps+1,1)=TableData(indTBest(idx,1):indTBest(idx,2),21);
    BrakeAEBValue.signals.values(1:SimulationSteps+1,1)=TableData(indTBest(idx,1):indTBest(idx,2),22);
    BrakeTSRValue.signals.values(1:SimulationSteps+1,1)=TableData(indTBest(idx,1):indTBest(idx,2),23);
    BrakeACCValue.signals.values(1:SimulationSteps+1,1)=TableData(indTBest(idx,1):indTBest(idx,2),24);
    checkVal.signals.values(1:SimulationSteps+1,1:5)= TableData(indTBest(idx,1):indTBest(idx,2),25:29);
    
    [CriticalFitnes, BranchVisitedWhenFailureOccurs,timeWhenBranchShouldbeVisitedAndFailureoccurs,timeWhenBranchVisitedWhenFailureoccurs]= localizeFaultyPath(SimulationSteps,NormFoverTime1,NormFoverTime2,NormFoverTime3,NormFoverTime4,NormFoverTime5,MinNormFoverTime1,MinNormFoverTime2,MinNormFoverTime3,MinNormFoverTime4,MinNormFoverTime5,Branch,BrakeOutput,BrakePPValue,BrakeAEBValue, BrakeTSRValue,BrakeACCValue,posBranchParent(x,:))
    
    fprintf(fid1, 'CriticalFitnes : %d \n', CriticalFitnes);
    fprintf(fid1, 'BranchVisitedWhenFailureOccurs : %d \n', BranchVisitedWhenFailureOccurs);
    fprintf(fid1, 'timeWhenBranchShouldbeVisitedAndFailureoccurs : %0.6f \n', timeWhenBranchShouldbeVisitedAndFailureoccurs);
    fprintf(fid1, 'timeWhenBranchVisitedWhenFailureoccurs : %0.6f \n', timeWhenBranchVisitedWhenFailureoccurs);
    
    %APPLY-MUTATION
    count=0;
    Thre=0.5^count;
    
    while rand(1) <= Thre ||count==0
        count=count+1;
        fprintf(fid1, 'count : %d \n', count);
        if count==1
            posBranchBestIntCToConsider(1,:)=posBranchParent(x,:);
        end
        if  count>1 
            posBranchBestIntCToConsider= posBranchP;
        end
        [pos, position1, position2, operator,posBranchP,ThrtoChange]=determineOperatorAndPositionFaultyPath4(posBranchBestIntCToConsider,BranchVisitedWhenFailureOccurs,checkVal,timeWhenBranchShouldbeVisitedAndFailureoccurs,timeWhenBranchVisitedWhenFailureoccurs);
        
        if operator ==3 && position1 <= BranchVisitedWhenFailureOccurs && position2 >= BranchVisitedWhenFailureOccurs
            BranchVisitedWhenFailureOccurs= BranchVisitedWhenFailureOccurs+1;
            
        end
        
        Thre=0.5^count;
        
        
        
        
        fprintf(fid1, 'pos : %d \n', pos);
        fprintf(fid1, 'position1 : %d \n', position1);
        fprintf(fid1, 'position2 : %d \n', position2);
        fprintf(fid1, 'operator : %d \n', operator);
        
        fprintf(fid1, 'ThrtoChange : %d \n', ThrtoChange);
        if operator ==0
            fprintf(fid1, 'Operator 0 \n');
        else
            
            
            open_system('Demo_TA2B_cs');
            sf = sfroot();
            Experiment_Name='Demo_TA2B';
            Simulink_ModelName= [ Experiment_Name '_cs'];
            a=find_system(Simulink_ModelName,'BlockType','SubSystem','Name','MATLAB Function1')
            block = sf.find('Path',a{1}','-isa','Stateflow.EMChart');
            
            fileScript= block.Script;
            dlmwrite('filetoextract.txt',fileScript, 'delimiter','');
            
            Tablefile=importdata('filetoextract.txt',' ');
            
            if isa(Tablefile,'struct')==1
                if strcmp(Tablefile.textdata{2},'')
                    th1=Tablefile.textdata{3};
                    th2=Tablefile.textdata{5};
                    th3=Tablefile.textdata{7};
                    th4=Tablefile.textdata{9};
                else
                    
                    th1=Tablefile.textdata{2};
                    th2=Tablefile.textdata{3};
                    th3=Tablefile.textdata{4};
                    th4=Tablefile.textdata{5};
                end
            else
                if strcmp(Tablefile{2},'')
                    th1=Tablefile{3};
                    th2=Tablefile{5};
                    th3=Tablefile{7};
                    th4=Tablefile{9};
                else
                    th1=Tablefile{2};
                    th2=Tablefile{3};
                    th3=Tablefile{4};
                    th4=Tablefile{5};
                end
            end
            
            
            s1= th1;
            a1=regexp(s1,'\d+\.?\d*|-\d+\.?\d*|\.?\d+|-\.?\d+','match');
            v1=str2double(cell2mat(a1));
            
           
            
            s2= th2;
            a2=regexp(s2,'\d+\.?\d*|-\d+\.?\d*|\.?\d+|-\.?\d+','match');
            v2=str2double(cell2mat(a2));
            
          
            
            s3= th3;
            a3=regexp(s3,'\d+\.?\d*|-\d+\.?\d*|\.?\d+|-\.?\d+','match');
            v3=str2double(cell2mat(a3));
            
            
            s4= th4;
            a4=regexp(s4,'\d+\.?\d*|-\d+\.?\d*|\.?\d+|-\.?\d+','match');
            v4=str2double(cell2mat(a4));
            
            
            if count==1
                UncThrAlpha=v1;
                BrkDistAlpha=v2;
                TTCThrAlpha=v3;
                BrDstAEBAlpha=v4;
                
                fprintf(fid1, 'initial values \n');
                
                fprintf(fid1, 'UncThrAlpha : %d \n', UncThrAlpha);
                fprintf(fid1, 'BrkDistAlpha : %d \n', BrkDistAlpha);
                fprintf(fid1, 'TTCThrAlpha : %d \n', TTCThrAlpha);
                fprintf(fid1, 'BrDstAEBAlpha : %d \n', BrDstAEBAlpha);
            else
                UncThrAlpha=UncThrAlpha;
                BrkDistAlpha=BrkDistAlpha;
                TTCThrAlpha=TTCThrAlpha;
                BrDstAEBAlpha=BrDstAEBAlpha;
            end
            
            if operator==1
                
                if posBranch(pos)==1
                    if ThrtoChange==1
                        UncThrAlpha= UncThrAlpha +((1+1)*rand(1,1)-1);%valrandom between [-1 .. 1]
                    end
                    if ThrtoChange==2
                        BrkDistAlpha= BrkDistAlpha + ((4+8)*rand(1,1)-8);%valrandom between [-8 .. 8]
                    end
                    if ThrtoChange==3
                        TTCThrAlpha= TTCThrAlpha +((3.5+3.5)*rand(1,1)-3.5);%valrandom between [-3.5 .. 3.5]
                    end
                end
                
                if posBranch(pos)==2
                    if ThrtoChange==4
                        UncThrAlpha= UncThrAlpha +((0.5+0.5)*rand(1,1)-0.5);%valrandom between [-0.5 .. 0.5]
                    end
                    
                end
                
                if posBranch(pos)==2 ||  posBranch(BranchVisitedWhenFailureOccurs)==2
                    
                    if ThrtoChange==5 %% we want to go inside b2, we should add a value to the threshold
                        BrDstAEBAlpha= BrDstAEBAlpha +((20-0)*rand(1,1)+0);%valrandom between [0 .. 20]
                    end
                end
            end
            fprintf(fid1, 'Modified values \n');
            
            fprintf(fid1, 'UncThrAlpha : %d \n', UncThrAlpha);
            fprintf(fid1, 'BrkDistAlpha : %d \n', BrkDistAlpha);
            fprintf(fid1, 'TTCThrAlpha : %d \n', TTCThrAlpha);
            fprintf(fid1, 'BrDstAEBAlpha : %d \n', BrDstAEBAlpha);
            
            
            %javaclasspath
            me=svv.parser.Mutation
            if count==1
                ModifiedIntC=me.readAndPrintFile(BestPar, pos, position1, position2, operator,UncThrAlpha,	 BrkDistAlpha, TTCThrAlpha,	 BrDstAEBAlpha);
            else
                ModifiedIntC=me.readAndPrintFile(PATH+'/Demo_TA2B/IntCToConsider.m', pos, position1, position2, operator,UncThrAlpha,	 BrkDistAlpha, TTCThrAlpha,	 BrDstAEBAlpha);
                
                
            end
            
            %%%%%%%%%%%%% Modify IntC %%%%%%%%%%%%%%
            open_system('Demo_TA2B_cs');
            sf = sfroot();
            Experiment_Name='Demo_TA2B';
            Simulink_ModelName= [ Experiment_Name '_cs'];
            a=find_system(Simulink_ModelName,'BlockType','SubSystem','Name','MATLAB Function1')
            block = sf.find('Path',a{1}','-isa','Stateflow.EMChart');
            
            fileId = fopen('ModifiedIntC.m');
            opPoints = fscanf(fileId, '%c');
            block.Script =sprintf('%c', opPoints);
            
            intcModifiedString =sprintf('%c', opPoints);
            
            fprintf(fid2, 'Iteration %d \n', iter);
            fprintf(fid2, '%c',opPoints);
            fprintf(fid2, '\n');
            
            fid = fopen('IntCToConsider.m', 'wt');
            fprintf(fid, '%s', intcModifiedString);
            fclose(fid);
            fprintf(fid1, 'posBranchParent %d %d %d %d %d %d %d \n', posBranchP);
            
        end
    end
    %%%%% RUN-EVALUATE
    if operator==0 && count==1
        fprintf(fid1, 'do nothing \n');
    else
        OF=0;
        tableData=[];
        cf=0;
        for i=1:size(Input,1)
            x0P=Input(i,1);
            y0P=Input(i,2);
            ThP=Input(i,3);
            v0P=Input(i,4);
            v0C=Input(i,5);
            z0P=0;
            v0L=Input(i,7);
            x0L= Input(i,8);
            
            %%%%%%%%% Generate the experiment %%%%%%%%%
            disp('Variables reinitialized...');
            disp('------------------------');
            startTime = rem(now,1);
            startTimeA = rem(now,1);
            Experiment_Name='Demo_TA2B'; % Hereby enter your experiment name
            PreScan_ExperimentName= [Experiment_Name '.pex']; % add the extension for the PreScan file
            Simulink_ModelName= [ Experiment_Name '_cs'];% add the extension for the Simulink file
            PreScan_ExperimentLocation = [pwd '\' PreScan_ExperimentName];
            k=0;
            A = cell(7,1);
            B = cell(7,1);
            NbrOfTagAndVal=0;
            
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='PositionX0P';
            radiusvalue={num2str(Input(i,1))};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='PositionY0P';
            radiusvalue={num2str(Input(i,2))};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='OrientationThP';
            radiusvalue={num2str(Input(i,3))};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            if Input(i,11)==1
                NbrOfTagAndVal=NbrOfTagAndVal+1;
                A{NbrOfTagAndVal}='EnableFog';
                enablefog = {'True'};
                clear idx currentName
                idx = randi(numel(enablefog),1) ;
                currentName = enablefog{idx};
                B{NbrOfTagAndVal}=currentName;
            else
                A{NbrOfTagAndVal}='EnableFog';
                enablefog = {'False'};
                clear idx currentName
                idx = randi(numel(enablefog),1) ;
                currentName = enablefog{idx};
                B{NbrOfTagAndVal}=currentName;
                
            end
            if Input(i,6)==-1
                NbrOfTagAndVal=NbrOfTagAndVal+1;
                A{NbrOfTagAndVal}='FogType';
                fogtype = {'DimGray'};
                clear idx currentName
                idx = randi(numel(fogtype),1) ;
                currentName = fogtype{idx};
                B{NbrOfTagAndVal}=currentName;
            end
            
            if Input(i,6)==0
                NbrOfTagAndVal=NbrOfTagAndVal+1;
                A{NbrOfTagAndVal}='FogType';
                fogtype = {'DarkGray'};
                clear idx currentName
                idx = randi(numel(fogtype),1) ;
                currentName = fogtype{idx};
                B{NbrOfTagAndVal}=currentName;
            end
            
            if Input(i,6)==1
                NbrOfTagAndVal=NbrOfTagAndVal+1;
                A{NbrOfTagAndVal}='FogType';
                fogtype = {'Silver'};
                clear idx currentName
                idx = randi(numel(fogtype),1) ;
                currentName = fogtype{idx};
                B{NbrOfTagAndVal}=currentName;
            end
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='StopSignX';
            radiusvalue={num2str(Input(i,9))};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='SpeedLimitX';
            radiusvalue={num2str(Input(i,10))};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            for m=1:NbrOfTagAndVal
                clear Tag Val
                Tag(m,:) =A{m};
                Val(m,:)=B{m};
                dos( ['PreScan.CLI.exe -convertAndLoad ' PreScan_ExperimentLocation ' -set ' Tag(m,:) '=' Val(m,:)]);     % Change the value in PreScan
                dos( 'PreScan.CLI.exe -save');         % Save the PreScan experiment
                
            end
            
            dos ('PreScan.CLI.exe --realignPaths');
            dos( 'PreScan.CLI.exe -build');
            
            open_system(Simulink_ModelName);                 % Open the Simulink Model
            
            regenButtonHandle = find_system(Simulink_ModelName, 'FindAll', 'on', 'type', 'annotation','text','Regenerate');
            regenButtonCallbackText = get_param(regenButtonHandle,'ClickFcn');
            
            eval(regenButtonCallbackText);
            
            xStopS=Input(i,9);
            
            %%%%%%%%% run the experiment %%%%%%%%%
            sim(fullfile(mfilepath,'/Demo_TA2B_cs.mdl'));
            
            %%%%%%%%%
            maxSimulationsPerViewer=5;
            
            closeViewer = mod(simulationCount + 1, maxSimulationsPerViewer) == 0; % Close viewer is true if Simulation is > than maxSimulationsPerViewer
            simulationCount = simulationCount + 1;
            
            % Close viewer, if needed,  to release memory
            if (closeViewer)
                visViewer = 5;
                maxWaitTimeMS = 10*1000;
                nl.tno.wt.prescan.utils.ProcessManagement.Stop(visViewer, maxWaitTimeMS);
                pause(0.5);
                nl.tno.wt.prescan.utils.ProcessManagement.Start(5, maxWaitTimeMS);
            end
            %%%%%%
            
            AA=SimStopTime.time;
            b=numel(AA);
            TotSim=AA(b);
            SimulationTimeStep=0.005;
            SimulationSteps=TotSim/SimulationTimeStep;
            xStop=Input(i,9);
            xSpeedLimit=Input(i,10);
            %%%% compute the fitness %%%%
            [MinNormFoverTime1 , MinNormFoverTime2 ,MinNormFoverTime3, MinNormFoverTime4 , MinNormFoverTime5,NormFoverTime1, NormFoverTime2, NormFoverTime3, NormFoverTime4, NormFoverTime5]=computeMinFOverTime3(desiredSpeed,Input(i,:),SimulationSteps,xPerson,xCar,yPerson, yCar, xLeadCar,xStop,xSpeedLimit,vCar,v0L);
            FTC(i)=MinNormFoverTime1 + MinNormFoverTime2 + MinNormFoverTime3 + MinNormFoverTime4 + MinNormFoverTime5;
            FitnessTC1(i)=MinNormFoverTime1;
            FitnessTC2(i)=MinNormFoverTime2;
            FitnessTC3(i)=MinNormFoverTime3;
            FitnessTC4(i)=MinNormFoverTime4;
            
            
            
            OF=OF+FTC(i);
            fprintf(fid1, 'FitnessInput %d ', i);
            fprintf(fid1, 'FitnessTC1 %0.6f \n', FitnessTC1(i));
            fprintf(fid1, 'FitnessTC2 %0.6f \n', FitnessTC2(i));
            fprintf(fid1, 'FitnessTC3 %0.6f \n', FitnessTC3(i));
            fprintf(fid1, 'FitnessTC4 %0.6f \n', FitnessTC4(i));
            
            
            
            if i==1
                tableData(1,1)=SimulationSteps;
                tableData(1:SimulationSteps+1,2)=NormFoverTime1;
                tableData(1:SimulationSteps+1,3)=NormFoverTime2;
                tableData(1:SimulationSteps+1,4)=NormFoverTime3;
                tableData(1:SimulationSteps+1,5)=NormFoverTime4;
                tableData(1:SimulationSteps+1,6)=NormFoverTime5;
                tableData(1,7)=MinNormFoverTime1;
                tableData(1,8)=MinNormFoverTime2;
                tableData(1,9)=MinNormFoverTime3;
                tableData(1,10)=MinNormFoverTime4;
                tableData(1,11)=MinNormFoverTime5;
                tableData(1:SimulationSteps+1,12:19)=Branch.signals.values(1:SimulationSteps+1,1:8);
                tableData(1:SimulationSteps+1,20)=BrakeOutput.signals.values(1:SimulationSteps+1,1);
                tableData(1:SimulationSteps+1,21)=BrakePPValue.signals.values(1:SimulationSteps+1,1);
                tableData(1:SimulationSteps+1,22)=BrakeAEBValue.signals.values(1:SimulationSteps+1,1);
                tableData(1:SimulationSteps+1,23)=BrakeTSRValue.signals.values(1:SimulationSteps+1,1);
                tableData(1:SimulationSteps+1,24)=BrakeACCValue.signals.values(1:SimulationSteps+1,1);
                tableData(1:SimulationSteps+1,25:29)=checkVal.signals.values(1:SimulationSteps+1,1:5);
                indT(i,1)=1;
                indT(i,2)=SimulationSteps+1;
            else
                SimulationSteps=size(NormFoverTime1,1) -1;
                indT(i,1)=size(tableData,1)+1;
                indT(i,2)=size(tableData,1)+SimulationSteps+1;
                
                tableData(indT(i,1),1)=SimulationSteps;
                tableData(indT(i,1):indT(i,2),2)=NormFoverTime1;
                tableData(indT(i,1):indT(i,2),3)=NormFoverTime2;
                tableData(indT(i,1):indT(i,2),4)=NormFoverTime3;
                tableData(indT(i,1):indT(i,2),5)=NormFoverTime4;
                tableData(indT(i,1):indT(i,2),6)=NormFoverTime5;
                tableData(indT(i,1),7)=MinNormFoverTime1;
                tableData(indT(i,1),8)=MinNormFoverTime2;
                tableData(indT(i,1),9)=MinNormFoverTime3;
                tableData(indT(i,1),10)=MinNormFoverTime4;
                tableData(indT(i,1),11)=MinNormFoverTime5;
                tableData(indT(i,1):indT(i,2),12:19)=Branch.signals.values(1:SimulationSteps+1,1:8);
                tableData(indT(i,1):indT(i,2),20)=BrakeOutput.signals.values(1:SimulationSteps+1,1);
                tableData(indT(i,1):indT(i,2),21)=BrakePPValue.signals.values(1:SimulationSteps+1,1);
                tableData(indT(i,1):indT(i,2),22)=BrakeAEBValue.signals.values(1:SimulationSteps+1,1);
                tableData(indT(i,1):indT(i,2),23)=BrakeTSRValue.signals.values(1:SimulationSteps+1,1);
                tableData(indT(i,1):indT(i,2),24)=BrakeACCValue.signals.values(1:SimulationSteps+1,1);
                tableData(indT(i,1):indT(i,2),25:29)=checkVal.signals.values(1:SimulationSteps+1,1:5);
            end
            critFit= checkInput(MinNormFoverTime1,MinNormFoverTime2,MinNormFoverTime3,MinNormFoverTime4,MinNormFoverTime5);
            if critFit~=0
                cf=cf+1;
                CRF(i)=1;
            else
                CRF(i)=0;
            end
        end
        
        fprintf(fid1, 'OF = %0.6f \n', OF);
        fprintf(fid1, 'number of failed test cases %d \n', cf);
        
        
        minFitnessTC1=min(FitnessTC1);
        minFitnessTC2=min(FitnessTC2);
        minFitnessTC3=min(FitnessTC3);
        minFitnessTC4=min(FitnessTC4);
        FitnessTC=[minFitnessTC1 minFitnessTC2 minFitnessTC3 minFitnessTC4];
        fprintf(fid1, 'Min FitnessTC %0.10f  %0.10f  %0.10f  %0.10f \n', FitnessTC);
        
        
        FitnessTCj=FitnessTC;
        FTCj=FTC;
        
        %%%Update Archive
        
        cc=0;
        isDominates=0;
        
        for j=1:TotalSizeArchive
            if IsInArchive(j)==1
                FitnessTCParentx=strcat('FitnessTCParent',int2str(j));
                FitP=eval(FitnessTCParentx)
                s1=0;
                for o=1:size(FitnessTCj,2)
                    if FitP(o)==FitnessTCj(o)
                        s1=s1+1;
                    end
                end
                if s1==size(FitnessTCj,2)
                    same=1;
                    break;
                else
                    same=0;
                end
            end
        end
        
        if same ==0
            for j=1:TotalSizeArchive
                if IsInArchive(j)==1
                    FitnessTCParentx=strcat('FitnessTCParent',int2str(j));
                    FitP=eval(FitnessTCParentx)
                    
                    
                    Ver= VerifyDominance(FitP, FitnessTCj);
                    if Ver==1
                        cc=cc+1;
                        IsInArchive(j)=0;
                    else
                        Ver1= VerifyDominance(FitnessTCj,FitP);
                        if Ver1==1
                            isDominates=1;
                        end
                    end
                end
            end
            if isDominates==0 || cc >0
                fprintf(fid1, 'Add an element to Archive \n');
                ADDBool=1;
                countImpro=0;
                TotalSizeArchive=TotalSizeArchive+1;
                %Add it to the archive
                %create parent x
                
                numP=TotalSizeArchive;
                posBranchParent(numP,:)=posBranchP;
                fprintf(fid1, 'Add Parent num %d \n', numP);
                fprintf(fid1, 'posBranchParent %d %d %d %d %d %d %d \n', posBranchParent(numP,:));
                
                IsInArchive(numP)=1;
                nameP=  strcat('Parent',int2str(numP),'.m');
                fid = fopen(nameP, 'wt');
                
                fprintf(fid, '%s', intcModifiedString);
                fclose(fid);
                
                BestFitCrit(:,1)=FTCj;
                BestFitCrit(:,2)=CRF;
                
                assignin('base',strcat('BestFitCritParent',int2str(numP)),BestFitCrit)
                
                
                tableDataALLBestx=strcat('tableDataALLBest',int2str(numP));
                nametable=strcat(tableDataALLBestx,'.txt');
                save(nametable,'tableData','-ascii')
                
               
                assignin('base',strcat('indTBestParent',int2str(numP)),indT)
                
               
                assignin('base',strcat('FitnessTCParent',int2str(numP)),FitnessTCj)
                
                
                
                nbrC=0;
                BestFitCritParentx=strcat('BestFitCritParent',int2str(numP));
                BValCritP=eval(BestFitCritParentx);
                
                for i=1:size(BValCritP,1)
                    if BValCritP(i,2)==1
                        nbrC=nbrC+1;
                    end
                end
                assignin('base',strcat('nbrFailingParent',int2str(numP)),nbrC);
                
                nbrFailingParentx=strcat('nbrFailingParent',int2str(numP));
                
                if  eval(nbrFailingParentx) < BestNumbFailing
                    BestNumbFailing= eval(nbrFailingParentx);
                    fprintf(fid1, 'BestNumbFailing test cases = %d \n', BestNumbFailing);
                end
                
            end
            
            
            
            SizeArc=0;
            for h=1:TotalSizeArchive
                if IsInArchive(h)==1
                    SizeArc=SizeArc+1;
                end
            end
            BestSizeArc=SizeArc;
            if countImpro==CounterRestart
                fprintf(fid1, 'countImpro = %d \n', countImpro);
            end
            if countImpro==CounterRestart && SizeArc==1
                %%restart archive
                fprintf(fid1, 'Restart Archive \n');
                IsInArchive(1)=1;
                for o=2:TotalSizeArchive
                    IsInArchive(o)=0;
                end
            end
            %%%%
            fprintf(fid1, 'Real Size of Arc = %d \n', SizeArc);
            
            
            fprintf(fid5, 'SimTicToc : %0.6f \n', SimTicToc);
            fprintf(fid5, 'Add an element to Archive \n');
            fprintf(fid5, 'Add Parent num %d \n', numP);
            fprintf(fid5, 'posBranchParent %d %d %d %d %d %d %d \n', posBranchParent(numP,:));
            fprintf(fid5, 'number of failed test cases %d \n', eval(nbrFailingParentx));
            fprintf(fid5, 'Min FitnessTC %0.6f  %0.6f  %0.6f  %0.6f \n', FitnessTCj);
            fprintf(fid5, 'TotalSizeArchive %d \n', TotalSizeArchive);
            fprintf(fid5, 'Real Size of Arc = %d \n', SizeArc);
            if numP==2
                fprintf(fid5, 'IsInArchive %d %d \n', IsInArchive(:));
            end
            if numP==3
                fprintf(fid5, 'IsInArchive %d %d %d\n', IsInArchive(:));
            end
            if numP==4
                fprintf(fid5, 'IsInArchive %d %d %d %d\n', IsInArchive(:));
            end
            if numP==5
                fprintf(fid5, 'IsInArchive %d %d %d %d %d\n', IsInArchive(:));
            end
            if numP==6
                fprintf(fid5, 'IsInArchive %d %d %d %d %d %d\n', IsInArchive(:));
            end
        else
            fprintf(fid1, 'Same as a previous one \n');
            if countImpro==CounterRestart && BestSizeArc==1
                %%restart archive
                fprintf(fid1, 'Restart Archive Many Same as a previous one\n');
                IsInArchive(1)=1;
                for o=2:TotalSizeArchive
                    IsInArchive(o)=0;
                end
            end
        end
    end
    SimTicToc=toc
    fprintf(fid1, 'SimTicToc : %0.6f \n', SimTicToc);
end